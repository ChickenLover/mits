import re
import json


def cleanse(text):
    return re.sub('[^\w\d ]', '', text).replace('_', ' ').replace('-', ' ')


def is_json_serializable(text):
    try:
        json.loads(text)
    except:
        return False
    else:
        return True
