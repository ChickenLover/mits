import time
import json

from telebot import types

from mits.bot import bot
from mits.db import DB

HW_CACHE = dict()


def handle_rate(message):
    cid = message.chat.id
    msg = bot.send_message(cid, 'CID SCORE')
    bot.register_next_step_handler(msg, handle_rate_second)


def handle_rate_second(message):
    db = DB()
    try:
        stud_id, score = message.text.split()
        db.users[str(stud_id)]['rating'] += int(score)
        bot.send_message(stud_id, f'Вам зачли домашнее задание! +{score} очков')
        time.sleep(0.33)
        bot.send_message(message.chat.id, 'Зачтено...')
    except Exception as e:
        bot.send_message(message.chat.id, 'oops, ' + str(e))
    finally:
        db.save()


def handle_notify(message):
    msg = bot.send_message(message.chat.id, 'Пришли мне текст и я разошлю его всем челикам')
    bot.register_next_step_handler(msg, handle_notify_second)


def handle_notify_second(message):
    db = DB()
    for cid, user in db.users.items():
        try:
            if user.get('notifications', True):
                bot.send_message(cid, f'Сообщение от преподавателя!\n\n{message.text}\n\nОтключить уведомления: /disable_notifications')
        except:
            pass
        time.sleep(0.1)


def handle_add_hw(message):
    cid = message.chat.id
    HW_CACHE[cid] = None
    msg = bot.send_message(cid, 'Отправь мне название домашнего задания')
    bot.register_next_step_handler(msg, handle_add_hw_title)


def handle_add_hw_title(message):
    cid = message.chat.id
    db = DB()
    title = message.text
    hw_id = db.new_hw(title, cid)
    HW_CACHE[cid] = hw_id
    db.save()
    msg = bot.send_message(cid, f'Отлично, теперь пришли описание задания')
    bot.register_next_step_handler(msg, handle_add_hw_description)


def handle_add_hw_description(message):
    cid = message.chat.id
    hw_id = HW_CACHE[cid]
    db = DB()
    description = message.text
    db.homeworks[hw_id]['description'] = description
    db.save()
    msg = bot.send_message(cid, f'Количество очков:')
    bot.register_next_step_handler(msg, handle_add_hw_score)


def handle_add_hw_score(message):
    cid = message.chat.id
    hw_id = HW_CACHE[cid]
    db = DB()
    score = message.text.strip()
    db.homeworks[hw_id]['score'] = int(score)
    db.save()
    markup = types.ReplyKeyboardMarkup(row_width=2)
    btn1 = types.KeyboardButton('Обязательное')
    btn2 = types.KeyboardButton('Дополнительное')
    markup.row(btn1, btn2)
    msg = bot.send_message(cid, f'Обязательное задание?', reply_markup=markup)
    bot.register_next_step_handler(msg, handle_add_hw_necessity)


def handle_add_hw_necessity(message):
    cid = message.chat.id
    hw_id = HW_CACHE[cid]
    db = DB()
    db.homeworks[hw_id]['required'] = message.text == 'Обязательное'
    db.save()
    msg = bot.send_message(cid, f'Добавлено!', reply_markup=types.ReplyKeyboardRemove(selective=False))


def handle_score_callback(call):
    data = json.loads(call.data)
    student = data['st']
    score = data['sc']
    hw_id = data['hw']
    db = DB()
    hw = db.homeworks.get(hw_id, {}).get('title')
    if hw_id in db.users[str(student)]['hws']:
        try:
            bot.answer_callback_query(call.id, text='Уже зачтено...')
            bot.edit_message_text(text=call.message.text + '\n\nЗачтено!',
                                          chat_id=call.message.chat.id,
                                          message_id=call.message.message_id,
                                          reply_markup=types.InlineKeyboardMarkup())
        except Exception as e:
            print(e)
    else:
        db.users[str(student)]['rating'] += int(score)
        db.users[str(student)]['hws'].append(hw_id) 
        db.save()
        bot.send_message(student, f'Вам зачли домашнее задание {hw}!\n+{score} очков!')
        try:
            bot.answer_callback_query(call.id, text='Зачтено...')
            bot.edit_message_text(text=call.message.text + '\n\nЗачтено!',
                                          chat_id=call.message.chat.id,
                                          message_id=call.message.message_id,
                                          reply_markup=types.InlineKeyboardMarkup())
        except Exception as e:
            print(e)


def handle_decline_callback(call):
    data = json.loads(call.data)
    student = data['st']
    hw_id = data['hw']
    db = DB()
    hw = db.homeworks.get(hw_id, {}).get('title')
    bot.send_message(student, f'Вам НЕ зачли домашнее задание {hw} :(\nЧто-то не так!')
    try:
        bot.answer_callback_query(call.id, text='Не зачтено...')
        bot.edit_message_text(text=call.message.text + '\n\nНе зачтено!',
                              chat_id=call.message.chat.id,
                              message_id=call.message.message_id)
    except Exception as e:
        print(e)
