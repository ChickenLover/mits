from mits.db import DB
from mits.bot import bot

from mits.utils import cleanse


def handle_nickname(message):
    cid = message.chat.id
    db = DB()
    nickname = db.get_nickname(cid) or message.chat.first_name
    msg = bot.send_message(cid, f"Привет, {nickname}! Какой nickname вы хотите?")
    bot.register_next_step_handler(msg, handle_nickname_second)


def handle_nickname_second(message):
    cid = message.chat.id
    db = DB()
    nickname = cleanse(message.text or '')[:15]
    if not nickname:
        bot.send_message(cid, "Вы ввели некорректный никнейм!")
        return
    db.change_nickname(message, nickname)
    db.save()
    bot.send_message(cid, f'Ник изменен на {nickname}')
