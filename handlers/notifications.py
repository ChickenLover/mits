from mits.db import DB
from mits.bot import bot


def handle_enable_notifications(message):
    cid = message.chat.id
    db = DB()
    db.maybe_add_user(message)
    db.users[str(cid)]['notifications'] = True
    bot.send_message(cid, "Вы будете получать уведомления. Чтобы отписаться /disable_notifications")
    db.save()


def handle_disable_notifications(message):
    cid = message.chat.id
    db = DB()
    db.maybe_add_user(message)
    db.users[str(cid)]['notifications'] = False
    bot.send_message(cid, "Вы больше не будете получать уведомлений. Чтобы вновь их активировать /enable_notifications")
    db.save()
