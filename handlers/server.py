from mits.db import DB
from mits.bot import bot

from mits.conf import SERVER_HOST

 
def handle_get_creds(message):
    cid = message.chat.id
    db = DB()
    db.maybe_add_user(message)
    account = db.get_user(cid)['account']
    mtext = '''Ваши данные для входа на сервер:
    *Login*: `{login}`
    *Password*: `{passw}`

    Используйте их для подключения к нашему серверу:
    `ssh {login}@{host}`'''
    msg = mtext.format(login=account[0], passw=account[1], host=SERVER_HOST)
    try:
        bot.send_message(cid, msg, parse_mode='Markdown')
    except Exception as e:
        print(e, 'while get creds')
