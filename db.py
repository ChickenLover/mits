import os
import json
import time
import uuid

from tabulate import tabulate

from mits.conf import RATINGS_PATH, ACCOUNTS_PATH
from mits.utils import cleanse


DEFAULT_DB = {
    "id_seq": 1,
    "users": {},
    "homework": {},
    "accounts": []
}


class DB:
    def __init__(self):
        if not os.path.exists(RATINGS_PATH):
            with open(RATINGS_PATH, 'w', encoding='utf-8') as f:
                db = DEFAULT_DB.copy()
                if os.path.exists(ACCOUNTS_PATH):
                    with open(ACCOUNTS_PATH, encoding='utf-8') as a:
                        db['accounts'] = [x.split(':') for x in a.read().split('\n') if x]
                json.dump(db, f)
        with open(RATINGS_PATH, encoding='utf-8') as f:
            self.db = json.load(f)

    @property
    def users(self):
        return self.db['users']

    @property
    def homeworks(self):
        return self.db['homework']

    @property
    def accounts(self):
        return self.db['accounts']

    def new_hw(self, title, cid):
        hid = str(uuid.uuid4())[:8]
        hw = {
                'id': hid,
                'owner': str(cid),
                'added_at': int(time.time()),
                'title': title,
                'description': '',
                'score': '',
                'required': False
             }
        self.homeworks[hid] = hw
        return hid

    def get_next_id(self):
        next_id = self.db['id_seq']
        self.db['id_seq'] += 1
        self.save()
        return next_id

    def get_user(self, cid):
        return self.users.get(str(cid), {})

    def get_nickname(self, cid):
        nickname = self.get_user(cid).get('name')
        if nickname:
            return cleanse(nickname)

    def maybe_add_user(self, message):
        if str(message.chat.id) not in self.users:
            self.new_user(message)
            self.save()

    def new_user(self, message, nickname=None):
        cid = message.chat.id
        nickname = nickname or (message.chat.first_name + ' ' + (message.chat.last_name or ''))
        user_id = self.get_next_id()
        user =  {
                    'id': user_id,
                    'cid': str(cid),
                    'name': nickname,
                    'rating': 0,
                    'telegram': message.chat.username or 'no_username',
                    'hws': list(),
                    'account': self.accounts[user_id - 1],
                    'is_schooler': False,
                }
        self.users[str(cid)] = user
        return user

    def change_nickname(self, message, nickname):
        cid = message.chat.id
        if str(cid) not in self.users:
            self.new_user(message, nickname=nickname)
        else:
            self.users[str(cid)]['name'] = nickname

    def sorted_users(self, schoolers=False):
        users = []
        for _, data in self.users.items():
            if schoolers and not data.get('is_schooler', False):
                continue
            users.append((data['cid'], data['name'], data['rating'], '@' + data["telegram"]))
        users_sorted = sorted(users, key=lambda x: int(x[2]), reverse=True)
        return [(i, *data) for i, data in enumerate(users_sorted, 1)]

    def ratings_pretty(self, schoolers=False):
        users = [data[:1] + data[2:] for data in self.sorted_users(schoolers)[:50]]
        return tabulate(users, headers=['Position', 'Name', 'Score', 'Telegram'])

    def user_rating(self, cid):
        return self.get_user(cid).get('rating')

    def user_position(self, cid, is_schooler=False):
        return [i for (i, u_cid, *x) in self.sorted_users(is_schooler) if str(u_cid) == str(cid)][0]

    def save(self):
        with open(RATINGS_PATH, 'w', encoding='utf8') as f:
            json.dump(self.db, f, indent=4, ensure_ascii=False)
