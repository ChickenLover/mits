from mits.db import DB
from mits.bot import bot

from telebot import types


def handle_ratings(message, is_schoolers=False):
    cid = message.chat.id
    db = DB()
    text = '```\n' + db.ratings_pretty(is_schoolers) + '\n```'
    nick = db.get_nickname(cid)
    if nick:
        user = db.users[str(cid)]
        if not is_schoolers or user['is_schooler']:
            rating = db.user_rating(cid)
            position = db.user_position(cid, is_schooler=is_schoolers)
            text = f'{nick}, твой рейтинг {rating}. Позиция в топе - {position}\n\n' + text
    try:
        bot.send_message(cid, text, parse_mode='Markdown')
    except Exception as e:
        print(e, 'while Ratings')


def handle_ratings_common(message):
    handle_ratings(message)


def handle_school_ratings(message):
    handle_ratings(message, is_schoolers=True)


def handle_schooler_reg(message):
    cid = message.chat.id
    markup = types.ReplyKeyboardMarkup()
    markup.row(types.KeyboardButton('Подтверждаю'))
    markup.row(types.KeyboardButton('Отмена'))
    try:
        msg = bot.send_message(cid, "Вы собираетесь зарегистрироваться в рейтинге школьников. Попасть в него можно только если вы еще не закончили школу (5-11-ый класс). Подтверждаете?",
                               reply_markup=markup)
    except Exception as e:
        print(e, 'while schooler registration')
    else:
        bot.register_next_step_handler(msg, handle_schooler_reg_confirm)


def handle_schooler_reg_confirm(message):
    cid = message.chat.id
    if message.text == 'Подтверждаю':
        db = DB()
        user = db.users.get(str(cid)) or db.new_user(message)
        user['is_schooler'] = True
        db.save()
        text = 'Вы зарегистрированы как школьник! Чтобы смотреть рейтинговую таблицу среди учеников школ, пишите /schools'
    else:
        text = 'Отменено'
    try:
        bot.send_message(cid, text, reply_markup=types.ReplyKeyboardRemove(selective=False))
    except Exception as e:
        print(e, 'while schooler registration 2')
