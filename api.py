#!/usr/bin/env python3
# coding=utf-8

import json

from mits.conf import ADMINS, ADMIN_USERNAME
from mits.bot import get_bot
from mits.utils import is_json_serializable
bot = get_bot()

from mits.handlers.notifications import handle_enable_notifications, handle_disable_notifications
from mits.handlers.nickname import handle_nickname
from mits.handlers.homework import handle_homework_handin, handle_homework_list
from mits.handlers.ratings import handle_ratings_common, handle_school_ratings, handle_schooler_reg
from mits.handlers.help import handle_welcome
from mits.handlers.server import handle_get_creds
from mits.handlers.admin import handle_rate, handle_add_hw, handle_notify,\
                                handle_score_callback, handle_decline_callback


def callback_action_func(action):
    return lambda call: is_json_serializable(call.data) and json.loads(call.data)['a'] == action


if __name__ == "__main__":
    print('starting...')

    private_only = lambda message: message.chat.type == 'private'
    admin_only = lambda message: message.chat.id in ADMINS
    bot.message_handler(commands=['start', 'help'], func=private_only)(handle_welcome)
    bot.message_handler(commands=['nickname'], func=private_only)(handle_nickname)
    bot.message_handler(commands=['handin'], func=private_only)(handle_homework_handin)
    bot.message_handler(commands=['hwlist'], func=private_only)(handle_homework_list)
    bot.message_handler(commands=['regschool'], func=private_only)(handle_schooler_reg)
    bot.message_handler(commands=['creds'], func=private_only)(handle_get_creds)

    bot.message_handler(commands=['ratings'])(handle_ratings_common)
    bot.message_handler(commands=['schools'])(handle_school_ratings)
    bot.message_handler(commands=['enable_notifications'])(handle_enable_notifications)
    bot.message_handler(commands=['disable_notifications'])(handle_disable_notifications)

    # Admin
    bot.message_handler(commands=['rate'], func=admin_only)(handle_rate)
    bot.message_handler(commands=['addhw'], func=admin_only)(handle_add_hw)
    bot.message_handler(commands=['notify'], func=admin_only)(handle_notify)
    bot.callback_query_handler(func=callback_action_func('score'))(handle_score_callback)
    bot.callback_query_handler(func=callback_action_func('decline'))(handle_decline_callback)

    bot.enable_save_next_step_handlers(delay=2)
    bot.load_next_step_handlers()

    bot.polling()
