import telebot

from mits.conf import TOKEN


bot = None

def get_bot():
    global bot
    if bot is None:
        bot = telebot.TeleBot(TOKEN, threaded=False)
    return bot
