import time
import json

from telebot import types

from mits.bot import bot
from mits.db import DB


hw_queues = dict()


def handle_homework_list(message):
    cid = message.chat.id
    db = DB()
    homeworks_done = db.get_user(str(cid)).get('hws')
    homeworks = sorted(db.homeworks.values(), key=lambda x: x['added_at'], reverse=True)
    text = '\n========================\n'.join(f"""{x['title']} {'(Сдано)' if homeworks_done and x['id'] in homeworks_done else ''}
{x['description']}
Очки: {"0xff" if x['score'] == -1 else x['score']}
Обязательное: {'Да' if x['required'] else 'Нет'}
""" for x in homeworks)
    try:
        bot.send_message(cid, text)
    except Exception as e:
        print(e, 'while homework')
    

def handle_homework_handin(message):
    cid = message.chat.id
    db = DB()
    homeworks_done = db.users.get(str(cid), {}).get('hws', [])
    homeworks = sorted(db.homeworks.values(), key=lambda x: x['added_at'], reverse=True)
    markup = types.ReplyKeyboardMarkup()
    for hw in homeworks:
        if hw['id'] not in homeworks_done:
            markup.row(types.KeyboardButton(hw['title']))
    try:
        msg = bot.send_message(cid, "Какую работу вы хотите сдать?", reply_markup=markup)
    except Exception as e:
        print(e, 'while homework')
    else:
        bot.register_next_step_handler(msg, handle_homework_choose)


def handle_homework_choose(message):
    cid = message.chat.id
    db = DB()
    title = message.text
    hws = [hw for hw in db.homeworks.values() if hw['title'] == title]
    if not hws:
        try:
            bot.send_message(cid, 'No such homework', reply_markup=types.ReplyKeyboardRemove(selective=False))
        except Exception as e:
            print(e)
        return
    hw = hws[0]
    homeworks_done = db.users.get(str(cid), {}).get('hws', [])
    if hw['id'] in homeworks_done:
        try:
            bot.send_message(cid, 'Вы уже сдали эту работу!', reply_markup=types.ReplyKeyboardRemove(selective=False))
        except Exception as e:
            print(e)
        return

    text = f'''*{hw['title']}*

{hw['description']}

Отправьте мне свою домашнюю работу. Когда закончите - пошлите /done
'''
    
    try:
        msg = bot.send_message(cid, text,
                               reply_markup=types.ReplyKeyboardRemove(selective=False))
        hw_queues[cid] = {'hw_id': hw['id'], 'items': list()}
    except Exception as e:
        print(e, 'while homework')
    else:
        bot.register_next_step_handler(msg, handle_homework_second)


def handle_homework_second(message):
    db = DB()
    cid = message.chat.id
    hw = db.homeworks[hw_queues[cid]['hw_id']]
    db.maybe_add_user(message)
    try:
        if message.text and message.text.startswith('/done'):
            bot.send_message(cid, "Задание сдано! Ожидайте пока преподаватель его проверит")
            time.sleep(0.33)
            bot.send_message(hw['owner'], 'Пересылаю дз...')
            time.sleep(0.33)
            for message in hw_queues[cid]['items']:
                bot.forward_message(hw['owner'], cid, message.message_id)
                time.sleep(0.33)
            del hw_queues[cid]
            user = db.users[str(cid)]
            text = f'''{hw['title']}
{hw['description']}
{message.chat.first_name or ""} {message.chat.last_name or ""} {user['name']} {user['rating']}pts {cid}'''
            markup = types.InlineKeyboardMarkup()
            data = {
                'st': cid,
                'hw': hw['id'],
                'sc': hw['score'],
            }
            decline_data = {**data, 'a': 'decline'}
            score_data = {**data, 'a': 'score'}
            markup.row(types.InlineKeyboardButton('Засчитать', callback_data=json.dumps(score_data)))
            markup.row(types.InlineKeyboardButton('Неверно', callback_data=json.dumps(decline_data)))
            bot.send_message(hw['owner'], text, reply_markup=markup)
        else:
            if cid not in hw_queues:
                del hw_queues[cid]
            hw_queues[cid]['items'].append(message)
            bot.register_next_step_handler(message, handle_homework_second)
    except Exception as e:
        print(e, 'while handing HW')
